# Contributor: Kali Prasad <kprasadvnsi@protonmail.com>
# Maintainer: Furkan Kardame <f.kardame@manjaro.org>

pkgname=uboot-orangepi5
pkgver=2024.10
pkgrel=1
pkgdesc='U-Boot for Orange Pi 5'
arch=('aarch64')
url='https://github.com/u-boot/u-boot'
license=('GPL')
makedepends=('wget' 'python' 'python-pyelftools' 'python-setuptools' 'bc' 'dtc' 'swig')
install=${pkgname}.install
_binsite="https://github.com/armbian/rkbin/raw"
_bincommit="c3017ce6efc9b5527262ae05de9331e73c76b7d4"
source=(
  "https://github.com/u-boot/u-boot/archive/refs/tags/v${pkgver}.tar.gz"
  "trusted-firmware-a.tar.gz::https://gitlab.collabora.com/hardware-enablement/rockchip-3588/trusted-firmware-a/-/archive/rk3588/trusted-firmware-a-rk3588.tar.gz"
  "rk3588_ddr.bin::$_binsite/$_bincommit/rk35/rk3588_ddr_lp4_2112MHz_lp5_2736MHz_v1.15.bin"
  "rk3588_bl31.elf::$_binsite/$_bincommit/rk35/rk3588_bl31_v1.45.elf"
)
md5sums=(
  SKIP
  SKIP
  SKIP
  SKIP
)

build() {
  unset CFLAGS CXXFLAGS CPPFLAGS LDFLAGS

  echo "Building trusted firmware"

  cd "${srcdir}/trusted-firmware-a-rk3588"
  make PLAT=rk3588 bl31

  echo "Building u-boot"

  cd "${srcdir}/u-boot-${pkgver}"
  make orangepi-5-rk3588s_defconfig

  export KCFLAGS='-Wno-error=address'
  export ROCKCHIP_TPL="${srcdir}/rk3588_ddr.bin"
  export BL31="${srcdir}/trusted-firmware-a-rk3588/build/rk3588/release/bl31/bl31.elf"
  make

  tools/mkimage -n rk3588 -T rksd -d ../rk3588_ddr.bin:spl/u-boot-spl.bin idbloader.img
}

package() {
  cd "${srcdir}/u-boot-${pkgver}"
  mkdir -p "${pkgdir}/boot/extlinux"
  install -D -m 0644 idbloader.img u-boot.itb -t "${pkgdir}/boot"
}
